﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GOB_Terminator
{
    /// <summary>
    /// Logique d'interaction pour IntroWindow.xaml
    /// </summary>
    public partial class IntroWindow : Window
    {
        public IntroWindow()
        {
            InitializeComponent();
        }

        #region Zone SQLite
        SQLiteConnection sqlite_conn;
        SQLiteCommand sqlite_cmd;
        SQLiteDataReader sqlite_datareader;
        string sqlite_path = "_system/t1000_matrix.db";
        //
        // SQLite CONNEXION
        private void SQL_Open()
        {
            sqlite_conn = new SQLiteConnection("Data Source=" + sqlite_path + ";Version=3;");
            sqlite_conn.Open();
        }
        #endregion

        #region Zone CHARGEMENT
        //
        // ERREURS
        private void Error_Box(string msg, int valeur)
        {
            if (valeur == 0)
            {
                System.Windows.Application.Current.Shutdown();
            }
            else if (valeur == 1)
            {
                MessageBox.Show(msg + Environment.NewLine + "Merci de réinstaller le logiciel.", "Erreur(s) / Error(s)", MessageBoxButton.OK, MessageBoxImage.Warning);
                System.Windows.Application.Current.Shutdown();
            }
        }
        //
        // SQLite TITLES
        private void SQL_titles(int myid)
        {
            try
            {
                SQL_Open();
                sqlite_cmd = sqlite_conn.CreateCommand();
                sqlite_cmd.CommandText = "SELECT * FROM titles WHERE id = " + myid + "";
                sqlite_cmd.ExecuteNonQuery();
                sqlite_datareader = sqlite_cmd.ExecuteReader();
                if (sqlite_datareader.HasRows == true)
                {
                    while (sqlite_datareader.Read())
                    {
                        txt_title.Text = sqlite_datareader["title"].ToString().ToUpper();
                        txt_name.Text = sqlite_datareader["name"].ToString().ToUpper() + " [" + sqlite_datareader["version"].ToString() + "]";
                        win_intro.Title = sqlite_datareader["company"].ToString() + " [" + sqlite_datareader["title"].ToString() + "] : " + sqlite_datareader["advertising"].ToString();
                    }
                }
                else
                {
                    Error_Box("SQL_Id = false", 1);
                }
                sqlite_conn.Close();
            }
            catch (Exception ex)
            {
                Error_Box(ex.Message, 1);
            }
        }
        //
        // VERSION
        private void Intro_version_Check(int myid)
        {
            try
            {
                WebClient client = new WebClient();
                Stream stream = client.OpenRead("http://bucketdead.ovh/gob_terminator/bucketdeadovh_version.txt");
                StreamReader reader = new StreamReader(stream);
                String ovh_version = reader.ReadToEnd();
                try
                {
                    SQL_Open();
                    sqlite_cmd = sqlite_conn.CreateCommand();
                    sqlite_cmd.CommandText = "SELECT * FROM titles WHERE id = " + myid + "";
                    sqlite_cmd.ExecuteNonQuery();
                    sqlite_datareader = sqlite_cmd.ExecuteReader();
                    if (sqlite_datareader.HasRows == true)
                    {
                        while (sqlite_datareader.Read())
                        {
                            if (ovh_version != sqlite_datareader["version"].ToString())
                            {
                                txt_error.Text = "Mise à jour " + ovh_version + " disponible !";
                            }
                            else
                            {
                                txt_error.Text = "";
                            }
                        }
                    }
                    else
                    {
                        Error_Box("SQL_Id = false", 1);
                    }
                    sqlite_conn.Close();
                }
                catch (Exception ex)
                {
                    Error_Box(ex.Message, 1);
                }
            }
            catch
            {
                txt_error.Text = "Pas de connexion...";
            }
        }
        //
        // LOGIN
        private void Intro_login(string pwd)
        {
            try
            {
                SQL_Open();
                sqlite_cmd = sqlite_conn.CreateCommand();
                sqlite_cmd.CommandText = "SELECT * FROM accounts WHERE pwd = '" + pwd + "' AND mod >= 1";
                sqlite_cmd.ExecuteNonQuery();
                sqlite_datareader = sqlite_cmd.ExecuteReader();
                if (sqlite_datareader.HasRows == true)
                {
                    while (sqlite_datareader.Read())
                    {
                        MainWindow mainWindow = new MainWindow();
                        mainWindow.Show();
                        this.Close();
                    }
                }
                else
                {
                    txt_error.Text = "Mot de passe incorrect";
                    pwb_login.Password = "";
                    pwb_login.IsEnabled = true;
                    btn_connecter.IsEnabled = true;
                }
                sqlite_conn.Close();
            }
            catch (Exception ex)
            {
                Error_Box(ex.Message, 1);
            }
        }
        //
        // ANIMATION
        private void Intro_animation(int seconde)
        {
            DoubleAnimation myDoubleAnimation = new DoubleAnimation();
            myDoubleAnimation.From = 1.0;
            myDoubleAnimation.To = 0.0;
            myDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(seconde));
            Storyboard myStoryboard = new Storyboard();
            myStoryboard.Children.Add(myDoubleAnimation);
            Storyboard.SetTargetName(myDoubleAnimation, rec_intro.Name);
            Storyboard.SetTargetProperty(myDoubleAnimation, new PropertyPath(Rectangle.OpacityProperty));
            myStoryboard.Begin(this);
        }
        //
        // TIMER
        DispatcherTimer dispatcherTimer = new DispatcherTimer();
        bool timer_first = true;
        private void Intro_timer(bool onoff)
        {
            if (onoff == true)
            {
                pgb_intro.Value = 0;
                if (timer_first == true)
                {
                    dispatcherTimer.Tick += new EventHandler(Intro_tick);
                    dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 10);
                }
                dispatcherTimer.Start();
            }
            else
            {
                dispatcherTimer.Stop();
            }
        }
        private void Intro_tick(object sender, EventArgs e)
        {
            pgb_intro.Value = pgb_intro.Value + 1;
            txt_error.Text = "Chargement : " + pgb_intro.Value.ToString() + " %";
            // LOGIN
            if (pgb_intro.Value >= pgb_intro.Maximum && timer_first == false)
            {
                Intro_timer(false);
                Intro_login(pwb_login.Password);
            }
            //
            else if (pgb_intro.Value == 1 && timer_first == true)
            {
                Intro_animation(5);
            }
            else if (pgb_intro.Value >= pgb_intro.Maximum && timer_first == true)
            {
                Intro_timer(false);
                timer_first = false;
                Intro_version_Check(1);
                pwb_login.IsEnabled = true;
                btn_connecter.IsEnabled = true;
            }
        }
        //
        // Win_intro CHARGEMENT
        private void Win_intro_Loaded(object sender, RoutedEventArgs e)
        {
            if (System.IO.File.Exists(sqlite_path))
            {
                SQL_titles(1);
                Intro_timer(true);
            }
            else
            {
                Error_Box(sqlite_path + " = false", 1);
            }
        }
        #endregion

        private void Btn_connecter_Click(object sender, RoutedEventArgs e)
        {
            if (pwb_login.Password.Length > 0)
            {
                pwb_login.IsEnabled = false;
                btn_connecter.IsEnabled = false;
                Intro_timer(true);
            }
            else
            {
                txt_error.Text = "Mot de passe incorrect";
            }
        }
    }
}
